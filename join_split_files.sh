#!/bin/bash

cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk.* 2>/dev/null >> system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk
rm -f system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat recovery.img.* 2>/dev/null >> recovery.img
rm -f recovery.img.* 2>/dev/null
cat odm/lib64/libxmi_high_dynamic_range_cdsp.so.* 2>/dev/null >> odm/lib64/libxmi_high_dynamic_range_cdsp.so
rm -f odm/lib64/libxmi_high_dynamic_range_cdsp.so.* 2>/dev/null
cat odm/lib64/libailab_rawhdr.so.* 2>/dev/null >> odm/lib64/libailab_rawhdr.so
rm -f odm/lib64/libailab_rawhdr.so.* 2>/dev/null
cat vendor_bootimg/06_dtbdump_Qualcomm_Technologies,_Inc._KalamaP_SoC.dtb.* 2>/dev/null >> vendor_bootimg/06_dtbdump_Qualcomm_Technologies,_Inc._KalamaP_SoC.dtb
rm -f vendor_bootimg/06_dtbdump_Qualcomm_Technologies,_Inc._KalamaP_SoC.dtb.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat vendor/bin/COSNet_spatial_8bit_quantized.serialized.bin.* 2>/dev/null >> vendor/bin/COSNet_spatial_8bit_quantized.serialized.bin
rm -f vendor/bin/COSNet_spatial_8bit_quantized.serialized.bin.* 2>/dev/null
cat vendor/lib64/libCOSNet_spatial_qnn_quantized.so.* 2>/dev/null >> vendor/lib64/libCOSNet_spatial_qnn_quantized.so
rm -f vendor/lib64/libCOSNet_spatial_qnn_quantized.so.* 2>/dev/null
cat bootRE/boot.elf.* 2>/dev/null >> bootRE/boot.elf
rm -f bootRE/boot.elf.* 2>/dev/null
cat product/app/TrichromeLibrary64/TrichromeLibrary64.apk.* 2>/dev/null >> product/app/TrichromeLibrary64/TrichromeLibrary64.apk
rm -f product/app/TrichromeLibrary64/TrichromeLibrary64.apk.* 2>/dev/null
cat product/app/Maps/Maps.apk.* 2>/dev/null >> product/app/Maps/Maps.apk
rm -f product/app/Maps/Maps.apk.* 2>/dev/null
cat product/app/WebViewGoogle64/WebViewGoogle64.apk.* 2>/dev/null >> product/app/WebViewGoogle64/WebViewGoogle64.apk
rm -f product/app/WebViewGoogle64/WebViewGoogle64.apk.* 2>/dev/null
cat product/app/MIUIVideoPlayer/MIUIVideoPlayer.apk.* 2>/dev/null >> product/app/MIUIVideoPlayer/MIUIVideoPlayer.apk
rm -f product/app/MIUIVideoPlayer/MIUIVideoPlayer.apk.* 2>/dev/null
cat product/app/YouTube/YouTube.apk.* 2>/dev/null >> product/app/YouTube/YouTube.apk
rm -f product/app/YouTube/YouTube.apk.* 2>/dev/null
cat product/app/Gmail2/Gmail2.apk.* 2>/dev/null >> product/app/Gmail2/Gmail2.apk
rm -f product/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat product/app/Photos/Photos.apk.* 2>/dev/null >> product/app/Photos/Photos.apk
rm -f product/app/Photos/Photos.apk.* 2>/dev/null
cat product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> product/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null >> product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk
rm -f product/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null
cat product/data-app/Duo/Duo.apk.* 2>/dev/null >> product/data-app/Duo/Duo.apk
rm -f product/data-app/Duo/Duo.apk.* 2>/dev/null
cat product/data-app/MIMediaEditorGlobalNuxi/MIMediaEditorGlobalNuxi.apk.* 2>/dev/null >> product/data-app/MIMediaEditorGlobalNuxi/MIMediaEditorGlobalNuxi.apk
rm -f product/data-app/MIMediaEditorGlobalNuxi/MIMediaEditorGlobalNuxi.apk.* 2>/dev/null
cat product/data-app/MISTORE_OVERSEA/MISTORE_OVERSEA.apk.* 2>/dev/null >> product/data-app/MISTORE_OVERSEA/MISTORE_OVERSEA.apk
rm -f product/data-app/MISTORE_OVERSEA/MISTORE_OVERSEA.apk.* 2>/dev/null
cat product/data-app/MIUINotes/MIUINotes.apk.* 2>/dev/null >> product/data-app/MIUINotes/MIUINotes.apk
rm -f product/data-app/MIUINotes/MIUINotes.apk.* 2>/dev/null
cat product/priv-app/ExtraPhotoGlobal/ExtraPhotoGlobal.apk.* 2>/dev/null >> product/priv-app/ExtraPhotoGlobal/ExtraPhotoGlobal.apk
rm -f product/priv-app/ExtraPhotoGlobal/ExtraPhotoGlobal.apk.* 2>/dev/null
cat product/priv-app/Gallery_M_Series_Global/Gallery_M_Series_Global.apk.* 2>/dev/null >> product/priv-app/Gallery_M_Series_Global/Gallery_M_Series_Global.apk
rm -f product/priv-app/Gallery_M_Series_Global/Gallery_M_Series_Global.apk.* 2>/dev/null
cat product/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null >> product/priv-app/Phonesky/Phonesky.apk
rm -f product/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null
cat product/priv-app/MIUISecurityCenterGlobal/MIUISecurityCenterGlobal.apk.* 2>/dev/null >> product/priv-app/MIUISecurityCenterGlobal/MIUISecurityCenterGlobal.apk
rm -f product/priv-app/MIUISecurityCenterGlobal/MIUISecurityCenterGlobal.apk.* 2>/dev/null
cat product/priv-app/Messages/Messages.apk.* 2>/dev/null >> product/priv-app/Messages/Messages.apk
rm -f product/priv-app/Messages/Messages.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null >> product/priv-app/MiuiCamera/MiuiCamera.apk
rm -f product/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null
cat product/priv-app/MIBrowserGlobal_builtin_before_2021/MIBrowserGlobal_builtin_before_2021.apk.* 2>/dev/null >> product/priv-app/MIBrowserGlobal_builtin_before_2021/MIBrowserGlobal_builtin_before_2021.apk
rm -f product/priv-app/MIBrowserGlobal_builtin_before_2021/MIBrowserGlobal_builtin_before_2021.apk.* 2>/dev/null
cat product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> product/priv-app/Velvet/Velvet.apk
rm -f product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
